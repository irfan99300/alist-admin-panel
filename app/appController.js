(function(){

    'use strict';

    var app = function($scope,appRequestApi,$state,alist_constant , $filter ,commonRequest){

        $scope.$parent.addClassForDataTable = function(){

            $('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
        }

        $scope.formatDate = function(request_date){

            return commonRequest.formatDate(request_date)        
        }

        $scope.imgUrl = alist_constant.imgPathForProfile;
        $scope.$parent.allCelebrityRequest = function(){

            var url = alist_constant.baseUrl + 'admin/getAllCelebrityRequest';

            appRequestApi.requestGet(url).then(function(res){
                var response = res;

                if(response.success){

                    $scope.celebrity_requests = response.data;
                }else{

                    alert(response.msg);
                }  
            })
        }

        $scope.$parent.getLoginInfo = function(){

            var url = alist_constant.baseUrl + 'user/getUserInfo?user_id=' + window.localStorage['user_id'];

            appRequestApi.requestGet(url).then(function(res){
                var response = res;
                if(response.success){

                    $scope.userProfile = response.data;
                }else{

                    alert(response.msg);
                }
            })
        }

        if(window.localStorage['auth']){

            $scope.$parent.allCelebrityRequest()
            $scope.$parent.getLoginInfo()
        }
        
        //console.log("app");
        $scope.logout = function(){
        	var data = {};

        	data.device_id = "dummy";
        	console.log("log")
            var url = 'http://localhost:3003/user/logout';
            var url = alist_constant.baseUrl + 'user/logout';


        	appRequestApi.requestPost(url,data).then(function(res){

                var response = res.data;

                if(response.success){

                    window.localStorage.clear();
                    $state.go('login' , {} , {'reload' : true}) 
                }else{

                    alert(response.msg);
                }
        		
        	})
        }

        $scope.goToUsers = function(type){

            $state.go( 'user' , {'type' : type} , {'reload' : true})
        }

    };

    app.$inject = ['$scope','appRequestApi','$state','alist_constant', '$filter','commonRequest'];
    
    angular
        .module('alist')
            .controller('AppController',app);

}());