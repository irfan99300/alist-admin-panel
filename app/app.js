'use strict';
var app = angular.module('alist', ['ui.router','ngSanitize']);

app.run(['$rootScope','$location','$state', function($rootScope, $location, $state) {

    $rootScope.location = $location;
    $rootScope.$on("$locationChangeStart", function(event, next, current) {

        if($location.path() == "/auth/login" && window.localStorage['auth']){


           $location.path('/admin/home')

        }else if(!window.localStorage['auth'] && $location.path().indexOf('/auth/content')){

           $location.path('/auth/login')
        }
    });
}]);

app.directive('pwCheck', [function () {
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.pwCheck;
        elem.add(firstPassword).on('keyup', function () {
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    }
}]);


app.config(function ($stateProvider,$urlRouterProvider,$qProvider,$locationProvider) {

  $qProvider.errorOnUnhandledRejections(false);
   $locationProvider.hashPrefix('');


        $stateProvider
        .state('login', {
            url: '/auth/login',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/login/login.html',
                    controller: 'Login'
                }
            }
        })

        .state('home', {
            url: '/admin/home',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/home/home.html',
                    controller: 'Home'
                }
            }
        })

        .state('user', {
            url: '/admin/user',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/user/user.html',
                    controller: 'User'
                }
            }
        })

        .state('fanDetail', {
            url: '/admin/fanDetail/:fan_id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/user/fanDetail.html',
                    controller: 'User'
                }
            }
        })

        .state('celebrity', {
            url: '/admin/celebrity',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/user/celebrity.html',
                    controller: 'Celebrity'
                }
            }
        })

        .state('celebrityDetail', {
            url: '/admin/celebrityDetail/:celebrity_id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/user/celebrityDetail.html',
                    controller: 'Celebrity'
                }
            }
        })

        .state('change-password', {
            url: '/admin/change-password',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/change_password/change_password.html',
                    controller: 'Password'
                }
            }
        })

        .state('notification', {
            url: '/admin/notification',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/notification/notification.html',
                    controller: 'Notification'
                }
            }
        })

        .state('profile', {
            url: '/admin/profile/:request_id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/userProfile/userProfile.html',
                    controller: 'Profile'
                }
            }
        })

        .state('createAdmin', {
            url: '/admin/createAdmin',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/createAdmin/createAdmin.html',
                    controller: 'Admin'
                }
            }
        })
        .state('list', {
            url: '/admin/list',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/createAdmin/adminList.html',
                    controller: 'Admin'
                }
            }
        })

        .state('updateAdmin', {
            url: '/admin/updateAdmin/:id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/createAdmin/editAdmin.html',
                    controller: 'Admin'
                }
            }
        })

        .state('promotionList', {
            url: '/admin/promotionList',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/promotions/promotions.html',
                    controller: 'Promotions'
                }
            }
        })

        .state('promotions', {
            url: '/admin/promotions/:celebrity_id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/promotions/promotions.html',
                    controller: 'Promotions'
                }
            }
        })
        .state('promotionDetail', {
            url: '/admin/promotionDetail/:id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/promotions/promotionDetail.html',
                    controller: 'PromotionDetail'
                }
            }
        })

        .state('postList', {
            url: '/admin/postList',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/post/post.html',
                    controller: 'Post'
                }
            }
        })

        .state('post', {
            url: '/admin/post/:celebrity_id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/post/post.html',
                    controller: 'Post'
                }
            }
        })

        .state('postDetail', {
            url: '/admin/postDetail/:id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/post/postDetail.html',
                    controller: 'PostDetail'
                }
            }
        })

        .state('help', {
            url: '/admin/help',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/help/help.html',
                    controller: 'Help'
                }
            }
        })

        .state('createHelp', {
            url: '/admin/createHelp',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/help/createHelp.html',
                    controller: 'Help'
                }
            }
        })

        .state('updateHelp', {
            url: '/admin/updateHelp/:id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/help/updateHelp.html',
                    controller: 'Help'
                }
            }
        })
        .state('manageContent', {
            url: '/admin/manageContent',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/manageContent/manageContent.html',
                    controller: 'manageContent'
                }
            }
        })
        .state('dynamicContent', {
            url: '/admin/dynamicContent/:id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/manageContent/dynamicContent.html',
                    controller: 'dynamicContent'
                }
            }
        })
        .state('content', {
            url: '/auth/content/:id',
            views: {
                'mainContent': {
                    templateUrl: 'app/templates/login/content.html',
                    controller: 'content'
                }
            }
        })

        if(window.localStorage['auth'])
            $urlRouterProvider.otherwise("/admin/home");
        else
            $urlRouterProvider.otherwise("/auth/login");
});           

//Used just so that the agular js accepts the _id from the Mongodb Users document
