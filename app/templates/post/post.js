(function(){

    'use strict';

    var promotions = function($scope , appRequestApi , $state , alist_constant , $stateParams,$filter , commonRequest){
 
        var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];
        console.log($stateParams.celebrity_id)
    	var table = $('#datatable').DataTable( {
    	    "processing": true,
    	    "serverSide": true,
            "pageLength": alist_constant.pageLimit,
            "order": [],
    	    "ajax":  {
    	        'url'  :  alist_constant.baseUrl +'admin/postList',
    	        "type" :  "POST",
    	        "headers" : {
    	        	"x-access-token" : header
    	        },
                "data": function ( d ) {
                    d.celebrity_id   = $stateParams.celebrity_id;
                }
    	    },
    	    "columns": [
                { "data": "celebrity_id.name" },
                { "data": "title" },
    	        { "data": "description" },
                { "data" : function (d) {

                    return commonRequest.formatDate(d.created_at)  
                },"searchable" : false, "orderable" : true },
                { "data" : function (d) {
                    var action
                    if(d.is_active == 2){

                        status = "<button data-post_id='"+d._id+"' class='posts btn btn-info'>View Detail</button>";
                    }else if(d.is_active == 1 || d.is_active == 0){

                        if(d.is_admin_blocked == 0){

                            status = '<button data-id="'+d._id+'" data-block_type="1" class="block btn btn-danger">Block</button>';
                                    
                        }else{

                            status = '<button data-id="'+d._id+'" data-block_type="0" class="block btn btn-success">Un block</button>';
                        }
                    }

                    return status
                },"searchable" : false},

                { "data" : function (d) {
                    
                    return '<button data-post_id="'+d._id+'" class="posts btn btn-info">View</button>';
                },"searchable" : false},
    	    ],
    	    "responsive": true
    	});
        
        //$('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
        $scope.$parent.addClassForDataTable()
        

    	$('.posts_dispalay').on('click' , '.block' , function(e){

    		var data = {};
    		data.post_id    = $(this).data('id');
    		data.block_type = $(this).data('block_type');
    		console.log(data)

    		var url = alist_constant.baseUrl + 'admin/postBlockUpdate';

    		appRequestApi.requestPost(url,data).then(function(res){

    		    var response = res.data;
    		    console.log(response)
    		    if(response.success){

    		    	swal({
    		    	  title: "Success",
    		    	  text: response.msg,
    		    	  confirmButtonClass: "btn-primary",
    		    	  confirmButtonText: "Ok",
    		    	  closeOnConfirm: true,
    		    	},
    		    	function(isConfirm) {
    		    	  if (isConfirm) {

    		    	    table.ajax.reload();
    		    	  }
    		    	});

    		    }else{

    		        swal(response.msg)
    		    }
    		})
    	})

        $('.posts_dispalay').on('click' , '.posts' , function(e){
            console.log($(this).data('post_id'))
            $state.go('postDetail' , {'id' : $(this).data('post_id')} , {'reload' : true});
        })
    };

    promotions.$inject = ['$scope' , 'appRequestApi' , '$state' , 'alist_constant', '$stateParams','$filter' , 'commonRequest'];
    
    angular
        .module('alist')
            .controller('Post',promotions);

}());