(function(){

    'use strict';

    var dynamicContent = function($scope , appRequestApi , $state,alist_constant ,$filter,$stateParams,$rootScope){
        console.log($stateParams.id);
        var url = alist_constant.baseUrl + 'content/getContent?content_type='+ $stateParams.id;
        var updateContentUrl = alist_constant.baseUrl + 'content/updateContent';
        $rootScope.loading_request = true
        appRequestApi.requestGet(url).then(function(res){
                $rootScope.loading_request = false
                console.log(res);
                if(res.success){
                   if($stateParams.id == 1){
                       $scope.tinymceModel = res.data.help_center;
                       $scope.title = "Help Center FAQ's";
                       $scope.callTinymce();
                   }else if($stateParams.id == 2){
                       $scope.tinymceModel = res.data.alist_promotion_requirement;
                       $scope.title = "Promotion Requirement";
                       $scope.callTinymce();
                   }else if($stateParams.id == 3){
                       $scope.tinymceModel = res.data.what_are_rewards;
                       $scope.title = "What are rewards";
                       $scope.callTinymce();
                   }else if($stateParams.id == 4){
                       $scope.tinymceModel = res.data.privacy_policy;
                       $scope.title = "Privacy Policy";
                       $scope.callTinymce();
                   }else if($stateParams.id == 5){
                       $scope.tinymceModel = res.data.terms_of_service;
                       $scope.title = "Terms of Services";
                       $scope.callTinymce();
                   }
                }
            })

        $scope.callTinymce  = function () {
          CKEDITOR.editorConfig = function (config) {
            config.language = 'es';
            config.uiColor = '#F7B42C';
            config.toolbarCanCollapse = true;

          };
          CKEDITOR.replace('editor1',{height:500});
              
        }
        $scope.updateContent  = function () {
            appRequestApi.requestPost(updateContentUrl,{'text':CKEDITOR.instances.editor1.getData(),'content_type':$stateParams.id}).then(function(res){
                console.log(res);
                if(res.data.success){
                   //'Success! successfully updated.'
                   swal("Successfully updated.");
                }else{
                    swal(res.data.msg)
                }
            })
        }

        
    };

    dynamicContent.$inject = ['$scope' , 'appRequestApi' , '$state','alist_constant','$filter','$stateParams','$rootScope'];
    
    angular
        .module('alist')
            .controller('dynamicContent',dynamicContent);

}());
