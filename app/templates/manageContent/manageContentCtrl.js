(function(){

    'use strict';

    var manageContent = function($scope , appRequestApi , $state,alist_constant ,$filter, $stateParams){
        $scope.edit = function(id) {
        	$state.go('dynamicContent',{'id':id} , {'reload' : true})
        }
        



    };

    manageContent.$inject = ['$scope' , 'appRequestApi' , '$state','alist_constant','$filter'];
    
    angular
        .module('alist')
            .controller('manageContent',manageContent);

}());