(function(){

    'use strict';

    var admin = function($scope , appRequestApi , $state,alist_constant , $parse , $stateParams , $rootScope){

        var table;

        $scope.list_init = function(){

            console.log("in")
            var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];
            
            table = $('.admin_display').DataTable( {
                "processing": true,
                "serverSide": true,
                "pageLength": alist_constant.pageLimit,
                "order": [],
                "ajax":  {
                    'url'  :  alist_constant.baseUrl +'admin/getAllUser',
                    "type" :  "POST",
                    "headers" : {
                        "x-access-token" : header
                    },
                    "data": function ( d ) {
                        d.user_type   = 3;
                    }
                },
                "columns": [
                    { "data": "name" },
                    { "data": "username" },
                    { "data": "email" },
                    { "data" : function (d) {

                        var blocked_status;

                        if(d.is_blocked == 1){

                            blocked_status = '<button data-id="'+d._id+'" data-block_type="'+d.is_blocked+'" class="block btn btn-primary">Un block</button>'
                        }else if(d.is_blocked == 2){

                            blocked_status = '<button data-id="'+d._id+'" data-block_type="'+d.is_blocked+'" class="block btn btn-danger">Block</button>'
                        }
                        return blocked_status
                    },"searchable" : false, "orderable" : false },
                    { "data" : function (d) {

                        return '<button data-id="'+d._id+'" class="admin-edit btn btn-info">Edit</button>';
                    },"searchable" : false, "orderable" : false }
                ],
                "responsive": true
            });
            //$('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
            $scope.$parent.addClassForDataTable()
        }

        $('.admin_display').on('click' , '.block' , function(e){

            var data = {};
            data.user_id = $(this).data('id');
            $(this).data('block_type') == 1 ? data.type = 2 : data.type = 1;

            console.log(data)

            var url = alist_constant.baseUrl + 'admin/adminBlockUpdate';

            appRequestApi.requestPost(url,data).then(function(res){

                var response = res.data;
                console.log(response)
                if(response.success){

                    swal({
                      title: "Success",
                      text: response.msg,
                      confirmButtonClass: "btn-primary",
                      confirmButtonText: "Ok",
                      closeOnConfirm: true,
                    },
                    function(isConfirm) {
                      if (isConfirm) {

                        table.ajax.reload();
                      }
                    });

                }else{

                    swal(response.msg)
                }
            })
        })

        $('.admin_display').on('click' , '.admin-edit' , function(e){

            $state.go('updateAdmin', { id : $(this).data('id')} , {'reload' : true});
        })

        $scope.edit_init = function(){

            var url = alist_constant.baseUrl + 'user/getUserInfo?user_id=' + $stateParams.id;

            appRequestApi.requestGet(url).then(function(res){

                if(res.success){

                    $scope.admin = res.data;
                    $scope.admin.password = ''
                }else{

                    swal(response.msg)
                }
            });
        }

        $scope.submit = function(){

            var url = alist_constant.baseUrl + 'auth/register';
            console.log($scope.admin);

            $scope.admin.is_admin = 1;
            $rootScope.loading_request = true
            appRequestApi.requestPost(url,$scope.admin).then(function(res){

                $rootScope.loading_request = false

                var response = res.data;
               // console.log(res.data.error.errors)
                if(response.success){

                    //swal("successfully created admin account");
                    $state.go("list" , {} , {'reload' : true});
                }else{

                    angular.forEach(response.error.errors , function(err , key){
               
                        var serverMessage = $parse(key+'.serverMessage');
                        serverMessage.assign($scope, err.message);
                    })
                }
            })
        }

        $scope.update = function(){

            var url = alist_constant.baseUrl + 'admin/adminupdateProfile';
            console.log($scope.admin);

            appRequestApi.requestPost(url, $scope.admin).then(function(res){

                var response = res.data;
               // console.log(res.data.error.errors)
                if(response.success){

                    swal({
                      title: "Success",
                      text: response.msg,
                      confirmButtonClass: "btn-primary",
                      confirmButtonText: "Ok",
                      closeOnConfirm: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            $state.go('list');
                        }
                    });
                }else{

                    swal(response.msg);
                }
            })
        }
    };

    admin.$inject = ['$scope', 'appRequestApi', '$state', 'alist_constant', '$parse','$stateParams','$rootScope'];
    
    angular
        .module('alist')
            .controller('Admin',admin);
}());