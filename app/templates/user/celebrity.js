(function(){

    'use strict';

    var celebrity = function($scope , appRequestApi , $state , alist_constant , $stateParams,commonRequest){
        
        var table
        var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];

        $scope.imgUrl = alist_constant.imgPathForProfile;

        $scope.formatDate = function(request_date){

            return commonRequest.formatDate(request_date)        
        }
        

        $scope.getCelebrityProfile = function(){

            var data = {};
            data.celebrity_id = $stateParams.celebrity_id;
            data.type         = 1;
            data.page_no      = 0;

            var url = alist_constant.baseUrl + 'celebrity/celebProfile';
      
            appRequestApi.requestPost(url, data).then(function(res){

                var response = res.data
                if(response.success){

                    $scope.celebrityDetail = response.celebrity

                    var url = alist_constant.baseUrl + 'fan/pointsfromEachFan?celebrity_id=' + $stateParams.celebrity_id;
                    
                    appRequestApi.requestGet(url).then(function(res){

                        if(res.success){

                            $scope.fanPoints = res.data
                            setTimeout(function(){ 

                                $('#points_table').DataTable({
                                    "pageLength": alist_constant.pageLimitInDetail,
                                });
                                $scope.$parent.addClassForDataTable()
                             }, 500);

/*                            var url = alist_constant.baseUrl + 'celebrity/getAllFanOfCelebrity?celebrity_id=' + $stateParams.celebrity_id;
                            
                            appRequestApi.requestGet(url).then(function(res){

                                if(res.success){

                                    $scope.allFans = res.data;
                                    setTimeout(function(){ 

                                        $('#fan_table').DataTable();
                                     }, 500);
                                }else{

                                    swal(res.msg)
                                }
                                console.log($scope.allFans);
                            });*/

                        }else{

                            swal(res.msg)
                        }
                        console.log($scope.fanPoints);
                    });
                }else{

                    swal(res.msg)
                }

               // console.log($scope.celebrityDetail)
            });
        }
        $scope.listing_init = function(){

            table = $('#datatable').DataTable( {
                "processing": true,
                "serverSide": true,
                "pageLength": alist_constant.pageLimit,
                "order": [],
                "ajax":  {
                    'url'  :  alist_constant.baseUrl +'admin/getAllUser',
                    "type" :  "POST",
                    "headers" : {
                        "x-access-token" : header
                    },
                    "data": function ( d ) {
                        d.user_type   = 1;
                    }
                },
                "columns": [
                    { "data": "username" },
                    { "data": "name" },
                    { "data": "email" },
                    { "data" : function (d) {
                        console.log(d.profile_pic)
                        if(d.profile_pic && d.profile_pic.indexOf('http') < 0){

                            return "<a data-fancybox='gallery' href='"+ $scope.imgUrl + d.profile_pic+"'><img height='80px' width='80px' src='"+ $scope.imgUrl + d.profile_pic+"'></a>";
                        }else if(d.profile_pic && d.profile_pic.indexOf('http') > -1){

                            return "<a data-fancybox='gallery' href='"+ d.profile_pic+"'><img height='80px' width='80px' src='"+d.profile_pic+"'></a>";
                        }else if(!d.profile_pic){

                            return "No profile picture"
                        }
                        //return $scope.formatDate(d.created_at);
                    
                    },"searchable" : false, "orderable" : true },
                    { "data" : function (d) {

                        var email_verified;

                        if(d.is_user_active == 0){

                            email_verified = "NO | " + '<button data-id="'+d._id+'" class="make-active btn btn-primary">Make Active</button>';;
                        }else if(d.is_user_active == 1){

                            email_verified = "YES";
                        }

                        return email_verified
                    },"searchable" : false, "orderable" : false },
                    { "data": "total_points" },
                    { "data": "fanLength"},
                    { "data" : function (d) {

                        return "<a href='#/admin/promotions/"+d._id+"'>Promotions</a> | <a href='#/admin/post/"+d._id+"'>Posts</a>"
                    },"searchable" : false, "orderable" : false },
                    { "data" : function (d) {

                        return commonRequest.formatDate(d.created_at)  
                    },"searchable" : false, "orderable" : true },
                    { "data" : function (d) {

                        var blocked_status;

                        if(d.is_blocked == 1){

                            blocked_status = '<button data-id="'+d._id+'" data-block_type="'+d.is_blocked+'" class="block btn btn-primary">Un block</button> ';
                        }else if(d.is_blocked == 2){

                            blocked_status = '<button data-id="'+d._id+'" data-block_type="'+d.is_blocked+'" class="block btn btn-danger">Block</button> ' ;
                        }

                        return blocked_status
                    },"searchable" : false, "orderable" : false },
                    { "data" : function (d) {

                        return  '<button data-id="'+d._id+'" class="profile btn btn-info">View</button>';

                    },"searchable" : false, "orderable" : false }

                ],
                "responsive": true
            });

            $scope.$parent.addClassForDataTable()
        }

       // $('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
        $('.users_dispalay').on('click' , '.profile' , function(e){
            
            console.log($(this).data('id'))
            $state.go('celebrityDetail' , {'celebrity_id' : $(this).data('id') } , {'reload' : true});
        });

    	$('.users_dispalay').on('click' , '.block' , function(e){

    		var data = {};
    		data.user_id = $(this).data('id');
    		$(this).data('block_type') == 1 ? data.type = 2 : data.type = 1;

    		console.log(data)

    		var url = alist_constant.baseUrl + 'admin/adminBlockUpdate';

    		appRequestApi.requestPost(url,data).then(function(res){

    		    var response = res.data;
    		    console.log(response)
    		    if(response.success){

    		    	swal({
    		    	  title: "Success",
    		    	  text: response.msg,
    		    	  confirmButtonClass: "btn-primary",
    		    	  confirmButtonText: "Ok",
    		    	  closeOnConfirm: true,
    		    	},
    		    	function(isConfirm) {
    		    	  if (isConfirm) {

    		    	    table.ajax.reload();
    		    	  }
    		    	});

    		    }else{

    		        swal(response.msg)
    		    }
    		})
    	})

    	$('.users_dispalay').on('click' , '.make-active' , function(e){

    		var data = {};
    		data.user_id = $(this).data('id');


    		console.log(data)

    		var url = alist_constant.baseUrl + 'admin/makeUserActive';

    		appRequestApi.requestPost(url,data).then(function(res){

    		    var response = res.data;
    		    console.log(response)
    		    if(response.success){

    		    	swal({
    		    	  title: "Success",
    		    	  text: response.msg,
    		    	  confirmButtonClass: "btn-primary",
    		    	  confirmButtonText: "Ok",
    		    	  closeOnConfirm: true,
    		    	},
    		    	function(isConfirm) {
    		    	  if (isConfirm) {

    		    	    table.ajax.reload();
    		    	  }
    		    	});

    		    }else{

    		        swal(response.msg)
    		    }
    		})
    	})

        $scope.getPromotions = function(id){

            if($scope.promotions == undefined){

                var url = alist_constant.baseUrl + 'admin/getCelebrityPosts?celebrity_id=' + $stateParams.celebrity_id +'&type=1';

                appRequestApi.requestGet(url).then(function(res){

                    if(res.success){

                        $scope.promotions = res.data;

                        setTimeout(function(){ 

                            $('#promotion_table').DataTable({
                                "pageLength": alist_constant.pageLimitInDetail,
                            });
                            $scope.$parent.addClassForDataTable()
                         }, 500);
                    }
                });  
            }
        }

        $scope.getPosts = function(id){

   
            if($scope.posts == undefined){

                var url = alist_constant.baseUrl + 'admin/getCelebrityPosts?celebrity_id=' + $stateParams.celebrity_id +'&type=0';

                appRequestApi.requestGet(url).then(function(res){

                    if(res.success){

                        $scope.posts = res.data;

                        setTimeout(function(){ 

                            $('#post_table').DataTable({
                                "pageLength": alist_constant.pageLimitInDetail,
                            });
                            $scope.$parent.addClassForDataTable()
                         }, 500);
                    }
                });  
            }
        }
    };

    celebrity.$inject = ['$scope' , 'appRequestApi' , '$state' , 'alist_constant', '$stateParams','commonRequest'];
    
    angular
        .module('alist')
            .controller('Celebrity',celebrity);

}());