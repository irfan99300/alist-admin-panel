(function(){

    'use strict';

    var user = function($scope , appRequestApi , $state , alist_constant , $stateParams ,commonRequest){

        $scope.imgUrl = alist_constant.imgPathForProfile;
        $scope.imgUrlForTrophy = alist_constant.imgPathForTrophy;
        var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];

        $scope.formatDate = function(request_date){

            return commonRequest.formatDate(request_date)        
        }

        $scope.getStarsForPromotions = function(){

            console.log($scope.promotionStars)
            if($scope.promotionStars == undefined){

                var url = alist_constant.baseUrl + 'fan/starsByFan?fan_id=' + $stateParams.fan_id;
                
                appRequestApi.requestGet(url).then(function(res){

                    if(res.success){

                        $scope.promotionStars = res.data;
                        setTimeout(function(){ 

                            $('#stars_table').DataTable({
                                    "pageLength": alist_constant.pageLimitInDetail,
                            });
                            $scope.$parent.addClassForDataTable()
                        }, 500);
                    }
                })
            }
        }

        $scope.getFanProfile = function(){

            console.log($stateParams.fan_id);
            var url = alist_constant.baseUrl + 'fan/fanProfile?fan_id=' + $stateParams.fan_id;
  
            appRequestApi.requestGet(url).then(function(response){

                if(response.success){

                    $scope.fanDetail = response.userdata;
                    $scope.trophies = [];

                    angular.forEach( response.trophies , function(trophy , key){

                        if(trophy.is_my_trophy.length > 0){

                            $scope.trophies.push(trophy);
                        }
                    });

                    var url = alist_constant.baseUrl + 'fan/pointsToEachCelebrity?fan_id=' + $stateParams.fan_id;
                    appRequestApi.requestGet(url).then(function(res){

                        if(response.success){

                            $scope.celebrityPoints = res.data;

                            setTimeout(function(){ 

                                $('#points_table').DataTable({
                                    "pageLength": alist_constant.pageLimitInDetail,
                                });
                                $scope.$parent.addClassForDataTable()
                            }, 500); 
                        }
                        console.log($scope.celebrityPoints)
                    });
                }else{

                    swal(response.msg)
                }
                console.log($scope.fanDetail)
            });
        }
 
        
    	var table = $('#datatable').DataTable( {
    	    "processing": true,
    	    "serverSide": true,
            "pageLength": alist_constant.pageLimit,
            "order": [],
    	    "ajax":  {
    	        'url'  :  alist_constant.baseUrl +'admin/getAllUser',
    	        "type" :  "POST",
    	        "headers" : {
    	        	"x-access-token" : header
    	        },
    	        "data": function ( d ) {
    	            d.user_type   = 2;
    	        }
    	    },
    	    "columns": [
    	        { "data": "username" },
    	        { "data": "name" },
    	        { "data": "email" },
                { "data" : function (d) {
                    console.log(d.profile_pic)
                    if(d.profile_pic && d.profile_pic.indexOf('http') < 0){

                        return "<a data-fancybox='gallery' href='"+ $scope.imgUrl + d.profile_pic+"'><img height='80px' width='80px' src='"+ $scope.imgUrl + d.profile_pic+"'></a>";
                    }else if(d.profile_pic && d.profile_pic.indexOf('http') > -1){

                        return "<a data-fancybox='gallery' href='"+ d.profile_pic+"'><img height='80px' width='80px' src='"+d.profile_pic+"'></a>";
                    }else if(!d.profile_pic){

                        return "No profile picture"
                    }
                    //return $scope.formatDate(d.created_at);
                
                },"searchable" : false, "orderable" : true },
    	        { "data" : function (d) {

    	            var email_verified;

    	            if(d.is_user_active == 0){

    	                email_verified = "NO | " + '<button data-id="'+d._id+'" class="make-active btn btn-primary">Make Active</button>';
    	            }else if(d.is_user_active == 1){

    	                email_verified = "YES";
    	            }

    	            return email_verified
    	        },"searchable" : false, "orderable" : false },
               // { "data": commonRequest.formatDate('created_at') },
                { "data" : function (d) {

                    return commonRequest.formatDate(d.created_at)  
                },"searchable" : false, "orderable" : true },
    	        { "data" : function (d) {

    	            var blocked_status;

    	            if(d.is_blocked == 1){

    	                blocked_status = '<button data-id="'+d._id+'" data-block_type="'+d.is_blocked+'" class="block btn btn-primary">Un block</button>';
    	            }else if(d.is_blocked == 2){

    	                blocked_status = '<button data-id="'+d._id+'" data-block_type="'+d.is_blocked+'" class="block btn btn-danger">Block</button>';
    	            }

    	            return blocked_status
    	        },"searchable" : false, "orderable" : false },
				{ "data" : function (d) {

					return	'<button data-id="'+d._id+'" class="profile btn btn-info">View Profile</button>';

				},"searchable" : false, "orderable" : false }
    	    ],
    	    "responsive": true
    	});

        //$('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
        $scope.$parent.addClassForDataTable()
        $('.users_dispalay').on('click' , '.profile' , function(e){
            
            console.log($(this).data('id'))
            $state.go('fanDetail' , {'fan_id' : $(this).data('id') } , {'reload' : true});
        });

    	$('.users_dispalay').on('click' , '.block' , function(e){

    		var data = {};
    		data.user_id = $(this).data('id');
    		$(this).data('block_type') == 1 ? data.type = 2 : data.type = 1;

    		console.log(data)

    		var url = alist_constant.baseUrl + 'admin/adminBlockUpdate';

    		appRequestApi.requestPost(url,data).then(function(res){

    		    var response = res.data;
    		    console.log(response)
    		    if(response.success){

    		    	swal({
    		    	  title: "Success",
    		    	  text: response.msg,
    		    	  confirmButtonClass: "btn-primary",
    		    	  confirmButtonText: "Ok",
    		    	  closeOnConfirm: true,
    		    	},
    		    	function(isConfirm) {

        		    	if (isConfirm) {
        		    	    table.ajax.reload();
        		    	}
    		    	});

    		    }else{

    		        swal(response.msg)
    		    }
    		})
    	})


    	$('.users_dispalay').on('click' , '.make-active' , function(e){

    		var data = {};
    		data.user_id = $(this).data('id');


    		console.log(data)

    		var url = alist_constant.baseUrl + 'admin/makeUserActive';

    		appRequestApi.requestPost(url,data).then(function(res){

    		    var response = res.data;
    		    console.log(response)
    		    if(response.success){

    		    	swal({
    		    	  title: "Success",
    		    	  text: response.msg,
    		    	  confirmButtonClass: "btn-primary",
    		    	  confirmButtonText: "Ok",
    		    	  closeOnConfirm: true,
    		    	},
    		    	function(isConfirm) {
    		    	  if (isConfirm) {

    		    	    table.ajax.reload();
    		    	  }
    		    	});

    		    }else{

    		        swal(response.msg)
    		    }
    		})
    	})

      
    };

    user.$inject = ['$scope' , 'appRequestApi' , '$state' , 'alist_constant', '$stateParams' ,'commonRequest'];
    
    angular
        .module('alist')
            .controller('User',user);

}());