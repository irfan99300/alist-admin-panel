(function(){

    'use strict';

    var profile = function($scope , appRequestApi , $state, alist_constant,$stateParams){

        $scope.imgUrl = alist_constant.imgPathForProfile;


        console.log($stateParams.request_id)
        $scope.getRequestInfo = function(){

            var url = alist_constant.baseUrl + 'admin/getRequestInfo?request_id=' + $stateParams.request_id;

            appRequestApi.requestGet(url).then(function(res){
                var response = res;
                if(response.success){

                    $scope.requestInfo = response.data;
                    console.log($scope.userProfile)
                }else{

                    alert(response.msg);
                }
            })
        }

        $scope.actionOnCelebRequest = function(request_id, user_id , action_type){

            var text;
            action_type == 2 ? text = "accept" : text = "decline";

            var data = {};
            data.user_id = user_id;
            data.type    = action_type;
            data.request_id    = request_id;

            swal({
              title: "",
              text: "Are you sure you want to "+text+" celebrity request??",
              confirmButtonClass: "btn-primary",
              confirmButtonText: "Yes!",
              showCancelButton: true,
              closeOnConfirm: true,
              closeOnCancel: true
            },
            function(isConfirm) {

                if (isConfirm) {

                    var url = alist_constant.baseUrl + 'celebrity/acceptCelebrity';
                    appRequestApi.requestPost(url, data).then(function(res){

                        var response = res.data;
                        if(response.success){

                            $scope.$parent.allCelebrityRequest();
                            $state.go('notification' , {} , {'reload' : true});
                        }else{

                            swal(response.msg)
                        }
                    })
                }
            });
        }
    };

    profile.$inject = ['$scope' , 'appRequestApi' , '$state','alist_constant','$stateParams'];
    
    angular
        .module('alist')
            .controller('Profile',profile);

}());