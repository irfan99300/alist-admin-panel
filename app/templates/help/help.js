(function(){

    'use strict';

    var help = function($scope , appRequestApi , $state,alist_constant , $parse , $stateParams){

        var table

        var socket = io.connect('http://localhost:3003');

        socket.emit('setUsername', { hello: 'world' });
        $scope.list_init = function(){

            var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];
            
            table = $('.help_dispalay').DataTable( {
                "processing": true,
                "serverSide": true,
                "pageLength": alist_constant.pageLimit,
                "order": [],
                "ajax":  {
                    'url'  :  alist_constant.baseUrl +'help/helpList',
                    "type" :  "POST",
                    "headers" : {
                        "x-access-token" : header
                    }
                },
                "columns": [
                    { "data": "question" },
                    { "data": "answer" },
                    { "data" : function (d) {

                         return '<button data-id="'+d._id+'" class="update btn btn-primary">Edit</button> '+
                                '<button data-id="'+d._id+'" class="delete btn btn-danger">Delete</button>'
                    },"searchable" : false, "orderable" : false },
                ],
                "responsive": true
            });
            //$('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
            $scope.$parent.addClassForDataTable()

        }

        $('.help_dispalay').on('click' , '.update' , function(){

            $state.go('updateHelp' , {'id' : $(this).data('id')} , {'reload' : true})
        })


        $('.help_dispalay').on('click' , '.delete' , function(){


            var id = $(this).data('id')
            swal({
              title: "",
              text: "Are you sure you want to delete this??",
              confirmButtonClass: "btn-primary",
              confirmButtonText: "Yes, delete it!",
              showCancelButton: true,
              closeOnConfirm: true,
              closeOnCancel: true
            },
            function(isConfirm) {

                if (isConfirm) {

                    var url = alist_constant.baseUrl + 'help/deleteHelp';
                    var data = {};
                    data.post_id = id
                    console.log(data)
                    appRequestApi.requestPost(url,data).then(function(res){
                        console.log(res)

                        var res = res.data
                        if(res.success){
                            console.log("inn")
                            table.ajax.reload();
                            $scope.$apply();

                        }else{

                            swal(response.msg);
                        }
                    })

                }
            })
        })

        $scope.edit_init = function(){

            var url = alist_constant.baseUrl + 'help/getById?help_id='+ $stateParams.id;

            appRequestApi.requestGet(url).then(function(res){

               // var response = res.data;
               // console.log(res.data.error.errors)
                if(res.success){

                    $scope.help = res.data
                }else{

                    swal(response.msg)
                }

            });
        }
            

        $scope.submit = function(){

            var url = alist_constant.baseUrl + 'help/addHelp';
            console.log($scope.help);

            appRequestApi.requestPost(url,$scope.help).then(function(res){

                var response = res.data;
               // console.log(res.data.error.errors)
                if(response.success){

                    swal({
                      title: "Success",
                      text: response.msg,
                      confirmButtonClass: "btn-primary",
                      confirmButtonText: "Ok",
                      closeOnConfirm: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            $state.go('help');
                        }
                    });
                }else{

                    /*angular.forEach(response.error.errors , function(err , key){
                        console.log(err.message)
                        var serverMessage = $parse(key+'.serverMessage');
                        serverMessage.assign($scope, err.message);
                    })*/
                    swal(response.msg);
                }
            })
        }

        $scope.update = function(){

            var url = alist_constant.baseUrl + 'help/updateHelp';
            console.log($scope.help);

            appRequestApi.requestPost(url, $scope.help).then(function(res){

                var response = res.data;
               // console.log(res.data.error.errors)
                if(response.success){

                    swal({
                      title: "Success",
                      text: response.msg,
                      confirmButtonClass: "btn-primary",
                      confirmButtonText: "Ok",
                      closeOnConfirm: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            $state.go('help');
                        }
                    });
                }else{

                    /*angular.forEach(response.error.errors , function(err , key){
                        console.log(err.message)
                        var serverMessage = $parse(key+'.serverMessage');
                        serverMessage.assign($scope, err.message);
                    })*/
                    swal(response.msg);
                }
            })
        }
    };

    help.$inject = ['$scope', 'appRequestApi', '$state', 'alist_constant', '$parse','$stateParams'];
    
    angular
        .module('alist')
            .controller('Help',help);
}());