(function(){

    'use strict';

    var notification = function($scope , appRequestApi , $state,alist_constant ,$filter){

        $scope.imgUrl = alist_constant.imgPathForProfile;


        $scope.formatDate = function(request_date){

            var today_date = new Date().getDate()
            var request_day = new Date(request_date).getDate()
            if(today_date == request_day){

                return moment(request_date).fromNow()
            }else{

                return $filter('date')(request_date, "dd/MM/yyyy");
            }
        }

        var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];
        
        var table = $('#datatable').DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": alist_constant.pageLimit,
            "order": [],
            "ajax":  {
                'url'  :  alist_constant.baseUrl +'admin/getAllCelebrityRequest',
                "type" :  "POST",
                "headers" : {
                    "x-access-token" : header
                }
            },
            "columns": [
                { "data": "from_id.name" },
                { "data": "from_id.username" },
                { "data": "from_id.email" },
                { "data" : function (d) {
                    console.log(d.from_id.profile_pic)
                    if(d.from_id.profile_pic && d.from_id.profile_pic.indexOf('http') < 0){

                        return "<a data-fancybox='gallery' href='"+ $scope.imgUrl + d.from_id.profile_pic+"'><img height='80px' width='80px' src='"+ $scope.imgUrl + d.from_id.profile_pic+"'></a>";
                    }else if(d.from_id.profile_pic && d.from_id.profile_pic.indexOf('http') > -1){

                        return "<a data-fancybox='gallery' href='"+d.from_id.profile_pic+"'><img height='80px' width='80px' src='"+d.from_id.profile_pic+"'></a>";
                    }else if(!d.from_id.profile_pic){

                        return "No profile picture"
                    }
                    //return $scope.formatDate(d.created_at);
                
                },"searchable" : false, "orderable" : true },
                { "data" : function (d) {

                    return $scope.formatDate(d.created_at);
          
                },"searchable" : false, "orderable" : true },
                { "data" : function (d) {

                    if(d.type == 0){

                        return '<button data-user_id="'+d.from_id._id+'" data-request_id="'+d._id+'" data-type="2" type="button" class="btn btn-success action">Accept</button>'+ 
                               '<button data-user_id="'+d.from_id._id+'" data-request_id="'+d._id+'" data-type="0" type="button" class="btn btn-danger action">Decline</button>';
                    }else if(d.type == 1){

                        return 'Accepted';
                    }else if(d.type == 2){

                        return 'Decline';
                    }
                },"searchable" : false, "orderable" : false },
                { "data" : function (d) {

                    return '<button data-request_id="'+d._id+'" type="button" class="btn btn-info userInfo">View</button>';
                },"searchable" : false, "orderable" : false }
            ],
            "responsive": true
        });
        //$('.dataTables_length select, .dataTables_filter input').addClass('form-control').addClass('input-sm');
        $scope.$parent.addClassForDataTable()
        
        
        $('.x_panel').on('click' , '.action' , function(e){

            var text;
            $(this).data('type') == 2 ? text = "accept" : text = "decline"

            var data = {};
            data.user_id  = $(this).data('user_id');
            data.request_id  = $(this).data('request_id');
            data.type     = $(this).data('type');
            console.log(data)

            var id = $(this).data('id')
            swal({
              title: "",
              text: "Are you sure you want to "+text+" celebrity request??",
              confirmButtonClass: "btn-primary",
              confirmButtonText: "Yes!",
              showCancelButton: true,
              closeOnConfirm: true,
              closeOnCancel: true
            },
            function(isConfirm) {

                if (isConfirm) {

                    var url = alist_constant.baseUrl + 'celebrity/acceptCelebrity';

                    appRequestApi.requestPost(url,data).then(function(res){

                        var response = res.data;
                        console.log(response)
                        if(response.success){

                            $scope.$parent.allCelebrityRequest();
                            table.ajax.reload();
                        }else{

                            swal(response.msg)
                        }
                    })

                }
            });
        })


        $('.x_panel').on('click' , '.userInfo' , function(e){

            $state.go('profile' , {'request_id' : $(this).data('request_id')} , {'reload' : true});
        })
    };

    notification.$inject = ['$scope' , 'appRequestApi' , '$state','alist_constant','$filter'];
    
    angular
        .module('alist')
            .controller('Notification',notification);

}());