(function(){

    'use strict';

    var home = function($scope , appRequestApi , $state , alist_constant , $stateParams , dateFilter,$rootScope){

      $(document).ready(function () {

        var h = window.innerHeight;
        console.log(h)
        $('.test_right').css('min-height',h);
  
      })

        $scope.home = function(){

            var url = alist_constant.baseUrl + 'admin/home';
            $rootScope.loading_request = true
            appRequestApi.requestGet(url).then(function(res){

            $rootScope.loading_request = false

                console.log(res)
                if(res.success){

                    $scope.home = res.data

                    console.log($scope.home)

                    var max = 0
                    var graphData = [];
                    var date = new Date();
                    var months = [];
                    for (var i = 0; i < 6; i++) {

                        
                        var counter = 0
                        months.push(dateFilter(date, 'MMM yyyy'));
                        for(var j = 0; j < $scope.home.users.length; j++){

                            var userssignupMonth = new Date($scope.home.users[j].created_at).getMonth();
               
                            if(userssignupMonth == date.getMonth()){

                                counter++
                            }
                        }
                        if(counter > max){

                            max = counter;
                        }
                        graphData.push(counter)
                        date.setMonth(date.getMonth() - 1)
                    }

                    var ctx = document.getElementById("myChart").getContext("2d");
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: months,
                            datasets: [{
                                label: 'value',
                                data: graphData,
                                lineTension : 0,
                                borderWidth: 4,
                                borderColor: "#3f82fa",
                            }],
                        },
                        options: {
                            "tooltips": {enabled: true},
                            "hover": {mode: null},
                            "animation": {
                              "duration": 1,
                              "onComplete": function() {
                                var chartInstance = this.chart,
                                  ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';
                                this.data.datasets.forEach(function(dataset, i) {
                                  var meta = chartInstance.controller.getDatasetMeta(i);
                                  meta.data.forEach(function(bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                  });
                                });
                              }
                            },
                            legend: {
                              "display": false
                            },
                            scales: {
                              yAxes: [{
                                display: true,
                                ticks: {
                                 // max: max,
                                  display: true,
                                  beginAtZero: true
                                }
                              }],
                              xAxes: [{
                             // type: 'time',
                              /*afterTickToLabelConversion: function(data){
                                  var xLabels = data.ticks;
                                  xLabels.forEach(function (labels, i) {
                                      if (i % 2 == 1){
                                          xLabels[i] = '';
                                      }
                                  });
                              }*/
                            }],
                            },
                            title: {
                                display: true,
                                text: 'Users per month'
                              }

                          }
                    });
                }else{

                    swal(res.msg)
                }

            })
        }      
    };

    home.$inject = ['$scope' , 'appRequestApi' , '$state' , 'alist_constant', '$stateParams' , 'dateFilter','$rootScope'];
    
    angular
        .module('alist')
            .controller('Home',home);

}());