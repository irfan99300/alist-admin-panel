(function(){

    'use strict';

    var promotionDetail = function($scope , appRequestApi , $state , alist_constant , $stateParams,$filter , $sce){

        $(document).ready(function () {
            $('#video a').fancybox({
                width: 640,
                height: 400,
                type: 'iframe'
            });
        });

        $scope.updateComment = function(comment_id , type,index){

            var data = {};
            data.type = type;
            data.comment_id = comment_id;
            var url = alist_constant.baseUrl + 'posts/updatePost';

            appRequestApi.requestPost(url,data).then(function(res){

                var response = res.data;
                console.log(response)
                if(response.success){

                    swal({
                      title: "Success",
                      text: response.msg,
                      confirmButtonClass: "btn-primary",
                      confirmButtonText: "Ok",
                      closeOnConfirm: true,
                    },
                    function(isConfirm) {

                        if (isConfirm) {
                           // comment_table.reload();
                           console.log(type)
                            $scope.comments[index].is_active = type;
                            $scope.$apply();
                        }
                    });

                }else{

                    swal(response.msg)
                }
            })
        }

        $scope.getComments = function(){

            console.log($scope.comments)
            if($scope.comments == undefined){

                var url = alist_constant.baseUrl + 'posts/getAllComments?post_id=' + $stateParams.id;

                appRequestApi.requestGet(url).then(function(res){

                    if(res.success){

                        $scope.comments = res.data;

                        setTimeout(function(){ 

                            $('#comments_table').DataTable({
                                "pageLength": alist_constant.pageLimitInDetail,
                            });
                            $scope.$parent.addClassForDataTable()
                         }, 500);
                    }
                });  
            }
        }
    
        $scope.postImgUrl = alist_constant.imgPathForPost;
        $scope.getPromotionInfo = function(){

            var url = alist_constant.baseUrl + 'posts/getPostById?post_id=' + $stateParams.id;

            appRequestApi.requestGet(url).then(function(res){
        
                if(res.success){

                    $scope.promotionDetail = res.data;

                    var url = alist_constant.baseUrl + 'fan/starsByFanToPromotion?post_id=' + $stateParams.id;

                    appRequestApi.requestGet(url).then(function(res){

                        if(res.success){

                            $scope.fanStars = res.data;

                            setTimeout(function(){ 

                                $('#stars_table').DataTable({
                                    "pageLength": alist_constant.pageLimitInDetail,
                                });
                                $scope.$parent.addClassForDataTable()
                             }, 500);
                        }
                    });
                }else{

                    swal(response.msg)
                }
                console.log($scope.promotionDetail);
            })
        } 

        $scope.trustSrc = function(src) {

           return $sce.trustAsResourceUrl($scope.postImgUrl+src);
        }

        $scope.actionOnPromotion = function(post_id , type){

            var data = {};
            data.post_id    = post_id;
            data.block_type = type;
            console.log(data)

            var url = alist_constant.baseUrl + 'admin/postBlockUpdate';

            appRequestApi.requestPost(url,data).then(function(res){

                var response = res.data;
                console.log(response)
                if(response.success){

                    swal({
                      title: "Success",
                      text: response.msg,
                      confirmButtonClass: "btn-primary",
                      confirmButtonText: "Ok",
                      closeOnConfirm: true,
                    },
                    function(isConfirm) {
                      if (isConfirm) {

                        $state.go('promotions' , {} , {'reload' : true})
                      }
                    });

                }else{

                    swal(response.msg)
                }
            })
        }
    };

    promotionDetail.$inject = ['$scope' , 'appRequestApi' , '$state' , 'alist_constant', '$stateParams','$filter','$sce'];
    
    angular
        .module('alist')
            .controller('PromotionDetail',promotionDetail);

}());