(function(){

    'use strict';

    var content = function($scope , appRequestApi , $state,alist_constant,$stateParams,$sce){

        console.log("content");
        $scope.getcontent = function(){
            var url = alist_constant.baseUrl + 'auth/getContent?content_type='+$stateParams.id;

            appRequestApi.requestGet(url).then(function(res){
                console.log(res);
                if(res.success){
                    if($stateParams.id == 1){
                       $scope.text = $sce.trustAsHtml(res.data.help_center);
                       $scope.title = "Help Center FAQ's";
                   }else if($stateParams.id == 2){
                       $scope.text = $sce.trustAsHtml(res.data.alist_promotion_requirement);
                       $scope.title = "Promotion Requirement";
                   }else if($stateParams.id == 3){
                       $scope.text = $sce.trustAsHtml(res.data.what_are_rewards);
                       $scope.title = "What are rewards";
                   }else if($stateParams.id == 4){
                       $scope.text = $sce.trustAsHtml(res.data.privacy_policy);
                       $scope.title = "Privacy Policy";
                   }else if($stateParams.id == 5){
                       $scope.text = $sce.trustAsHtml(res.data.terms_of_service);
                       $scope.title = "Terms of Services";
                   }
                    
                    
                }else{

                    $scope.serverMsg = res.msg
                }
            })
        }
        
    };

    content.$inject = ['$scope' , 'appRequestApi' , '$state','alist_constant','$stateParams','$sce'];
    
    angular
        .module('alist')
            .controller('content',content);

}());