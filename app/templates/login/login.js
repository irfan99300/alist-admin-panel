(function(){

    'use strict';

    var login = function($scope , appRequestApi , $state,alist_constant){

       // console.log("login");
        $scope.submit = function(){

            var url = alist_constant.baseUrl + 'auth/adminLogin';

            appRequestApi.requestPost(url,$scope.login).then(function(res){

                var response = res.data;

                if(response.success){

                    window.localStorage['token'] = response.token;
                    window.localStorage['auth']  = true;
                    window.localStorage['user_id']  = response.user._id;
                    $state.go('home' , {} , {reload : true})
                    $scope.$parent.allCelebrityRequest()
                    $scope.$parent.getLoginInfo()
                }else{

                    $scope.serverMsg = response.msg
                }
            })
        }
        
        $scope.contentPage = function(id){
            $state.go('content',{'id':id} , {'reload' : true});
        }
    };

    login.$inject = ['$scope' , 'appRequestApi' , '$state','alist_constant'];
    
    angular
        .module('alist')
            .controller('Login',login);

}());