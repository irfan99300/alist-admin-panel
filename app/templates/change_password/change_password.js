(function(){

    'use strict';

    var password = function($scope , appRequestApi , $state , alist_constant , $stateParams){

        $scope.submit = function(){

            var url = alist_constant.baseUrl + 'user/changePassword';
            console.log($scope.formField);
            appRequestApi.requestPost(url,$scope.formField).then(function(res){

                var response = res.data;

  

                    swal(response.msg)
              
            })  
        }  
    };

    password.$inject = ['$scope' , 'appRequestApi' , '$state' , 'alist_constant', '$stateParams'];
    
    angular
        .module('alist')
            .controller('Password',password);

}());