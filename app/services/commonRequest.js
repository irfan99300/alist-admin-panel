(function(){
    'use strict';

    angular.module('alist').factory('commonRequest',['$filter',commonRequest]);

    function commonRequest($filter){

        function formatDate(request_date){

            var today_date = new Date().getDate()
            var request_day = new Date(request_date).getDate()
            if(today_date == request_day){

                return moment(request_date).fromNow()
            }else{

                return $filter('date')(request_date, "dd/MM/yyyy");
            }
        }

        return{
            formatDate  : formatDate
        };
    }

})();