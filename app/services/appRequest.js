(function(){
    'use strict';

    angular.module('alist').factory('appRequestApi',['$http','$q','$timeout','$state',appRequestApi]);

    function appRequestApi($http,$q,$timeout,$state){

        var myPopup;

        function requestPost(link,data){
            var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];
            var deferred = $q.defer();

            var config = {
                timeout: deferred.promise,
                headers : {
                    'Content-Type': 'application/json',
                    "x-access-token" : header
                }
            };


            $http.post(link,data, config)
                .then(function(data, status, headers, config){
                      
                    deferred.resolve(data);
                },function(reject){
                    
    
                    // error handler
                    if(reject.status == 401 || reject.status == 400){
                        console.log(reject);
                        var msg = reject.data == 'blocked' ? 'Blocked by admin' : 'Please login';
                    }
                    var error_msg = 'An error has occurred. Please try again...';
                    deferred.resolve(reject.data);
                });


            $timeout(function() {
                deferred.resolve('status'); // this aborts the request!
            }, 30000);
            return deferred.promise;
        }

        function requestGet(link){
     
            var header = (window.localStorage.getItem('token') == null) ? '' : window.localStorage['token'];
            var deferred = $q.defer();

            var  headers = {
                'Content-Type': 'application/json',
                "x-access-token" : header
            };

            $http.get(link, { timeout: deferred.promise, 'headers' : headers})
                .then(function(data,status, headers, config){
  
                    deferred.resolve(data.data);
                },function(reject){
                    var error_msg = 'An error has occurred. Please try again...';
                    deferred.resolve(reject.data);
                });

            $timeout(function() {
                deferred.resolve('status'); // this aborts the request!
            }, 30000);
            return deferred.promise;
        }

        return{
            requestPost  : requestPost,
            requestGet   : requestGet
        };
    }

})();