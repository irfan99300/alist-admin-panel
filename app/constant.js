'use strict';

var app = angular.module('alist')
.constant('alist_constant' , {
	'baseUrl' : 'http://172.16.2.7:3003/',
	'imgPathForProfile' : 'http://172.16.2.7:3003/profileImgs/',
	'imgPathForPost' : 'http://172.16.2.7:3003/postImgs/',
	'imgPathForTrophy' : 'http://172.16.2.7:3003/trophyImgs/',
	'pageLimit' : 50,
	'pageLimitInDetail' : 25
})
/*.constant('alist_constant' , {
	'baseUrl' : 'http://104.238.72.196:3003/',
	'imgPathForProfile' : 'http://104.238.72.196:3003/profileImgs/',
	'imgPathForPost' : 'http://104.238.72.196:3003/postImgs/',
	'imgPathForTrophy' : 'http://104.238.72.196:3003/trophyImgs/',
	'pageLimit' : 50,
	'pageLimitInDetail' : 25
});*/


